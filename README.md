# CAN signaling binder

CAN bus binder, based upon OpenXC vi-firmware project.

Full document can be found under `docs` directory.

# Fast build procedure

Just use autobuild script:

```bash
./autobuild/agl/autobuild build
./autobuild/agl/autobuild package
```

This will build both projects under build directory for each of them with default configuration.
